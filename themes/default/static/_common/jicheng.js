var jicheng = {
  togglePanel(willShow) {

    if (typeof willShow !== "boolean") {
      willShow = !!panel.hidden;
    }

    if (willShow) {
      panel.hidden = false;
      document.querySelector('#panel-close').focus();
    } else {
      panel.hidden = true;
      this.applyOptions();
    }
  },

  applyOptions() {
    let useAncient = false;
    if (document.body.getAttribute('data-type') === 'book') {
      // meta 非古版者不允許切換為古版
      useAncient = (() => {
        let metaElem = jicheng.mainElem.querySelector('.元[data-name="古版"]');
        if (metaElem) {
          let metaBool = typeof metaElem.value !== 'undefined' ? 
              metaElem.value !== 'false' : 
              metaElem.textContent !== '非古版';

          if (!metaBool) {
            return false;
          }
        }

        let mode = document.querySelector('#useAncient').value;

        switch (mode) {
          case "true":
            return true;
          case "false":
            return false;
          default:
            return false;
        }
      })();
    }

    let useVertical = false;
    {
      let mode = document.querySelector('#useVertical').value;

      switch (mode) {
        case "true":
          useVertical = true;
          break;
        case "false":
          useVertical = false;
          break;
        default:
          // 古版自動使用直書，除非 meta 設定為非直書
          let metaElem = jicheng.mainElem.querySelector('.元[data-name="直書"]');
          if (metaElem) {
            let metaBool = typeof metaElem.value !== 'undefined' ? 
                metaElem.value !== 'false' : 
                metaElem.textContent !== '非直書';
            useVertical = !!useAncient && metaBool;
          } else {
            useVertical = !!useAncient;
          }
          break;
      }
    }

    let useFont = false;
    {
      let mode = document.querySelector('#useFont').value;

      switch (mode) {
        case "default":
          useFont = false;
          break;
        default:
          // @TODO: 根據情況自動判斷
          useFont = !!useAncient;
          break;
      }
    }

    let fontSize = 16;
    {
      let size = document.querySelector('#fontSize').value;
      fontSize = parseInt(size, 10);
    }

    let useZhu = true;
    if (document.body.getAttribute('data-type') === 'book') {
      let mode = document.querySelector('#useZhu').value;

      switch (mode) {
        case "true":
          useZhu = true;
          break;
        case "false":
          useZhu = false;
          break;
        default:
          // @TODO: 根據情況自動判斷
          useZhu = true;
          break;
      }
    }

    let useShu = true;
    if (document.body.getAttribute('data-type') === 'book') {
      let mode = document.querySelector('#useShu').value;

      switch (mode) {
        case "true":
          useShu = true;
          break;
        case "false":
          useShu = false;
          break;
        default:
          // @TODO: 根據情況自動判斷
          useShu = true;
          break;
      }
    }

    let useJiao = true;
    if (document.body.getAttribute('data-type') === 'book') {
      let mode = document.querySelector('#useJiao').value;

      switch (mode) {
        case "true":
          useJiao = true;
          break;
        case "false":
          useJiao = false;
          break;
        default:
          // @TODO: 根據情況自動判斷
          useJiao = !useAncient;
          break;
      }
    }

    let useNumbering = 'none';
    if (document.body.getAttribute('data-type') === 'book') {
      let mode = document.querySelector('#useNumbering').value;

      switch (mode) {
        case "none":
        case "inline":
        case "hanging":
          useNumbering = mode;
          break;
        default:
          // @TODO: 根據情況自動判斷
          useNumbering = !useAncient ? "hanging" : "none";
          break;
      }
    }

    this.setAncientMode(useAncient);
    this.setVerticalMode(useVertical);
    this.setFontMode(useFont);
    this.setFontSize(fontSize);
    this.setZhuMode(useZhu);
    this.setShuMode(useShu);
    this.setJiaoMode(useJiao);
    this.setNumberingMode(useNumbering);
  },

  /**
   * 切換古版
   */
  setAncientMode(useAncient) {
    const isAncient = document.documentElement.classList.contains('古版');

    if (useAncient) {
      if (!isAncient) {
        this.markPunctuations();
        document.documentElement.classList.add('古版');
      }
    } else {
      if (isAncient) {
        document.documentElement.classList.remove('古版');
      }
    }
  },

  /**
   * 切換直書模式
   */
  setVerticalMode(useVertical) {
    const isVertical = document.documentElement.classList.contains('直書');

    this.setVerticalMode.keyboardHandler = this.setVerticalMode.keyboardHandler || function (event) {
      if ([event.ctrlKey, event.altKey, event.shiftKey, event.metaKey].some(x => !!x)) { return; }
      switch (event.which) {
        case 33: // PageUp
          window.scrollBy($(window).width(), 0);
          event.preventDefault();
          break;
        case 34: // PageDown
          window.scrollBy(-$(window).width(), 0);
          event.preventDefault();
          break;
        case 35: // End
          window.scrollTo(-$(document).width(), 0);
          event.preventDefault();
          break;
        case 36: // Home
          window.scrollTo(0, 0);
          event.preventDefault();
          break;
      }
    };

    this.setVerticalMode.mouseWheelHandler = this.setVerticalMode.mouseWheelHandler || function (event) {
      if ([event.ctrlKey, event.altKey, event.shiftKey, event.metaKey].some(x => !!x)) { return; }
      window.scrollBy(event.deltaY * event.deltaFactor, 0);
      event.preventDefault();
    };

    if (useVertical) {
      if (!isVertical) {
        document.documentElement.classList.add('直書');
        $(window).on('keydown', this.setVerticalMode.keyboardHandler);
        $('main').on('mousewheel', this.setVerticalMode.mouseWheelHandler);
      }
    } else {
      if (isVertical) {
        document.documentElement.classList.remove('直書');
        $(window).off('keydown', this.setVerticalMode.keyboardHandler);
        $('main').off('mousewheel', this.setVerticalMode.mouseWheelHandler);
      }
    }
  },

  /**
   * 切換字體
   */
  setFontMode(enable) {
    if (!this.setFontMode.css) {
      const css = this.setFontMode.css = document.createElement('style');
      css.textContent = `\
article {
  font-family: "DroidSansFallback", "WenQuanYi Zen Hei", "Heiti TC", "PMingLiU", san-serif;
}`;
    }

    if (enable) {
      if (!this.setFontMode.css.parentNode) {
        document.querySelector('head').appendChild(this.setFontMode.css);
      }
    } else {
      if (this.setFontMode.css.parentNode) {
        this.setFontMode.css.remove();
      }
    }
  },

  setFontSize(fontSize) {
    if (!this.setFontSize.css) {
      const css = this.setFontSize.css = document.createElement('style');
      document.querySelector('head').appendChild(css);
    }

    this.setFontSize.css.textContent = `html { font-size: ${fontSize}px; }`;
  },

  /**
   * 切換註、疏、校、編號
   */
  setZhuMode(enable) {
    if (!this.setZhuMode.css) {
      const css = this.setZhuMode.css = document.createElement('style');
      css.textContent = `.註 { display: none; }`;
    }

    if (enable) {
      if (this.setZhuMode.css.parentNode) {
        this.setZhuMode.css.remove();
      }
    } else {
      if (!this.setZhuMode.css.parentNode) {
        document.querySelector('head').appendChild(this.setZhuMode.css);
      }
    }
  },

  setShuMode(enable) {
    if (!this.setShuMode.css) {
      const css = this.setShuMode.css = document.createElement('style');
      css.textContent = `.疏 { display: none; }`;
    }

    if (enable) {
      if (this.setShuMode.css.parentNode) {
        this.setShuMode.css.remove();
      }
    } else {
      if (!this.setShuMode.css.parentNode) {
        document.querySelector('head').appendChild(this.setShuMode.css);
      }
    }
  },

  setJiaoMode(enable) {
    if (!this.setJiaoMode.css) {
      const css = this.setJiaoMode.css = document.createElement('style');
      css.textContent = `.校 { display: none; }`;
    }

    if (enable) {
      if (this.setJiaoMode.css.parentNode) {
        this.setJiaoMode.css.remove();
      }
    } else {
      if (!this.setJiaoMode.css.parentNode) {
        document.querySelector('head').appendChild(this.setJiaoMode.css);
      }
    }
  },

  setNumberingMode(mode) {
    if (!this.setNumberingMode.css) {
      const css = this.setNumberingMode.css = document.createElement('style');
      css.textContent = `.編號 { display: none; }`;

      // 計算編號總數及編號數字所需寬度
      const count = this.mainElem.querySelectorAll('.編號').length;
      const width = 0.5 * (count.toString(10).length + 1);

      const cssHanging = this.setNumberingMode.cssHanging = document.createElement('style');
      cssHanging.textContent = count <= 0 ? "" : `\
html:not(.直書) body > main {
  padding: 0 ${Math.max(width - 1.25, 0)}em;
}

html:not(.直書) .編號 {
  position: absolute;
  margin-top: .1em;
  margin-left: -${width + 0.5}em;
  width: ${width + 0.25}em;
  text-align: right;
}

html.直書 .編號 {
  position: absolute;
  margin-top: -1em;
}
`;
    }

    switch (mode) {
      case "inline": {
        if (this.setNumberingMode.css.parentNode) {
          this.setNumberingMode.css.remove();
        }
        if (this.setNumberingMode.cssHanging.parentNode) {
          this.setNumberingMode.cssHanging.remove();
        }
        break;
      }
      case "hanging": {
        if (this.setNumberingMode.css.parentNode) {
          this.setNumberingMode.css.remove();
        }
        if (!this.setNumberingMode.cssHanging.parentNode) {
          document.querySelector('head').appendChild(this.setNumberingMode.cssHanging);
        }
        break;
      }
      default: {
        if (!this.setNumberingMode.css.parentNode) {
          document.querySelector('head').appendChild(this.setNumberingMode.css);
        }
        if (this.setNumberingMode.cssHanging.parentNode) {
          this.setNumberingMode.cssHanging.remove();
        }
        break;
      }
    }
  },

  markPunctuations() {
    const rPunctuations = /[，、。·．‧；：？！…《》〈〉「」『』‘’“”〝〞‵′]+/i;
    const excludeSelector = 'del, ins, div[class~="元資料"], script, style, textarea';

    const replaceTextNode = (node) => {
      var s = node.nodeValue, m = s.match(rPunctuations), nextNode = null;
      if (m) {
        const replaceStart = m.index;
        const replaceLen = m[0].length;
        const wordNode = node.splitText(replaceStart);  // Y=X.splitText(index) 後X為前面Y為後面

        if (wordNode.nodeValue.length > replaceLen) {
          nextNode = wordNode.splitText(replaceLen);
        }

        const newNode = document.createElement('ins');
        newNode.textContent = m[0];
        wordNode.parentNode.replaceChild(newNode, wordNode);
      }
      return nextNode;
    };

    const processObject = (node) => {
      const excludeTags = new Set(node.querySelectorAll(excludeSelector));
      const list = [node];  // 待處理陣列，由後往前，0-based
      let i = 0;
      do {
        node = list[i];
        if (node.nodeType === 3) {
          let nextNode = replaceTextNode(node);
          if (nextNode) { list[i++] = nextNode };
        } else if (node.nodeType === 1) {
          if (node.hasChildNodes() && !excludeTags.has(node)) {
            let childs = node.childNodes, j = childs.length;
            while(j) { list[i++] = childs[--j]; }
          }
        }
      } while (i--);
    };

    if (!this.markPunctuations.done) {
      this.markPunctuations.done = true;

      processObject(this.mainElem);

      if (!this.mainElemShown.hidden) {
        processObject(this.mainElemShown);
      }
    }
  },

  generateToc() {
    const titleElemsExclude = new Set(this.mainElem.querySelectorAll(
      'header h1, header h2, header h3, header h4, header h5, header h6'
    ));

    const titleElems = Array.from(this.mainElem.querySelectorAll('h1, h2, h3, h4, h5, h6'))
        .filter(x => !titleElemsExclude.has(x));

    const contentElemsMap = this.contentElemsMap = new Map();
    titleElems.forEach((elem) => {
      let curElem = elem;
      while (curElem.parentNode.nodeName.toLowerCase() !== 'main') {
        curElem = curElem.parentNode;
      }
      contentElemsMap.set(elem, curElem);
    });

    const root = document.querySelector('#panel-main-toc-list');
    let prevElem = root;
    let prevLevel = titleElems.reduce((curValue, titleElem) => {
      const level = parseInt(titleElem.nodeName.match(/(\d+)$/)[1], 10);
      return Math.min(curValue, level);
    }, Infinity);

    // 全文
    {
      const ol = document.createElement('ol');
      root.appendChild(ol);
      const li = document.createElement('li');
      ol.appendChild(li);

      const a = document.createElement('a');
      a.href = 'javascript:';
      a.textContent = '（全文）';
      a.addEventListener('click', (event) => {
        this.loadSection(false);
      });
      li.appendChild(a);

      prevElem = li;
    }

    // 首節
    {
      const li = document.createElement('li');
      prevElem.parentNode.appendChild(li);

      const a = document.createElement('a');
      a.href = 'javascript:';
      a.textContent = '（首節）';
      a.addEventListener('click', (event) => {
        this.loadSection(null);
      });
      li.appendChild(a);

      prevElem = li;
    }

    titleElems.forEach((titleElem) => {
      const level = parseInt(titleElem.nodeName.match(/(\d+)$/)[1], 10);
      const li = document.createElement('li');

      if (level > prevLevel) {
        const ol = document.createElement('ol');
        prevElem.appendChild(ol);
        ol.appendChild(li);
      } else if (level < prevLevel) {
        let curElem = prevElem;
        while (prevLevel > level) {
          curElem = curElem.parentNode.parentNode;
          prevLevel--;
        }
        curElem.parentNode.appendChild(li);
      } else {
        prevElem.parentNode.appendChild(li);
      }

      const a = document.createElement('a');
      a.textContent = titleElem.textContent;
      a.href = 'javascript:';
      a.addEventListener('click', (event) => {
        this.loadSection(titleElem);
      });
      li.appendChild(a);

      prevElem = li;
      prevLevel = level;
    });
  },

  // titleElem - false: 顯示全文, null: 顯示首節, 其他: 顯示該標題元素所在節
  loadSection(titleElem) {
    if (titleElem === false) {
      // 顯示全文
      this.mainElemShown.hidden = true;
      this.mainElem.hidden = false;
    } else {
      let prevTitleElem = null;
      let nextTitleElem = null;
      let contentElem = null;
      let nextContentElem = null;

      if (!titleElem) {
        // 首節
        contentElem = this.mainElem.firstChild;

        const item = this.contentElemsMap.entries().next();
        if (!item.done) {
          nextTitleElem = item.value[0];
          nextContentElem = item.value[1];
        }
      } else {
        contentElem = this.contentElemsMap.get(titleElem);

        const iter = this.contentElemsMap.entries();
        do {
          item = iter.next();
          if (item.done) { break; }
          if (item.value[0] === titleElem) { break; }
          prevTitleElem = item.value[0];
        } while (true)

        item = iter.next();

        if (!item.done) {
          nextTitleElem = item.value[0];
        }

        while (!item.done) {
          if (item.value[1] !== contentElem) {
            nextContentElem = item.value[1];
            break;
          }

          item = iter.next();
        }
      }

      // 設定要顯示的元素
      {
        this.mainElemShown.innerHTML = '';

        let curElem = contentElem;
        while(curElem && curElem !== nextContentElem) {
          this.mainElemShown.appendChild(curElem.cloneNode(true));
          curElem = curElem.nextSibling;
        }
      }

      // 建立「上一節」「下一節」
      {
        const footerElem = document.createElement('footer');

        if (prevTitleElem) {
          const div = document.createElement('div');
          div.textContent = '上一節：';
          footerElem.appendChild(div);

          const a = document.createElement('a');
          a.textContent = prevTitleElem.textContent;
          a.href = 'javascript:';
          a.addEventListener('click', (event) => {
            this.loadSection(prevTitleElem);
          });
          div.appendChild(a);
        } else if (titleElem) {
          const div = document.createElement('div');
          div.textContent = '上一節：';
          footerElem.appendChild(div);

          const a = document.createElement('a');
          a.textContent = '（首節）';
          a.href = 'javascript:';
          a.addEventListener('click', (event) => {
            this.loadSection(null);
          });
          div.appendChild(a);
        } else {
          const div = document.createElement('div');
          div.textContent = '上一節：無';
          footerElem.appendChild(div);
        }

        if (nextTitleElem) {
          const div = document.createElement('div');
          div.textContent = '下一節：';
          footerElem.appendChild(div);

          const a = document.createElement('a');
          a.textContent = nextTitleElem.textContent;
          a.href = 'javascript:';
          a.addEventListener('click', (event) => {
            this.loadSection(nextTitleElem);
          });
          div.appendChild(a);
        } else {
          const div = document.createElement('div');
          div.textContent = '下一節：無';
          footerElem.appendChild(div);
        }

        this.mainElemShown.appendChild(footerElem);
      }

      this.mainElem.hidden = true;
      this.mainElemShown.hidden = false;
    }

    window.scrollTo(0, 0);
    this.togglePanel(false);
  },
};

document.addEventListener('DOMContentLoaded', (event) => {
  jicheng.panelElem = document.querySelector('#panel');
  jicheng.mainElem = document.querySelector('main');

  jicheng.mainElemShown = document.createElement('main');
  jicheng.mainElem.parentNode.insertBefore(jicheng.mainElemShown, jicheng.mainElem);

  var elem = document.querySelector('#panel-open-wrapper');
  if (elem) {
    elem.hidden = false;
  }

  var elem = document.querySelector('#panel-open');
  if (elem) {
    elem.href = "javascript:";
    elem.addEventListener('click', (event) => {
      jicheng.togglePanel(true);
    });
  }

  var elem = document.querySelector('#panel-close');
  if (elem) {
    elem.href = "javascript:";
    elem.addEventListener('click', (event) => {
      jicheng.togglePanel(false);
    });
  }

  if (document.body.getAttribute('data-type') !== 'book') {
    var elem = document.querySelector('#panel-main-view');
    if (elem) {
      elem.hidden = true;
    }
  }

  jicheng.generateToc();
  jicheng.applyOptions();
});
